# FOP
A framework for function optimization.

## Build
Requires C++17 (for fold expressions and general convenience).

If you are compiling for the first time :
``sh
cd build
cmake ..
make
``

Else :
``sh
cd build
make
``

## Optimizing functions
All the elements are defined in the optimizer namespace.

Both a `function` to optimize and an `optimization_method` should be defined
for obvious reasons. Optimizing is great and everything, but it can last for
quite some time. It is therefore nice to be updated on the current state of
progress of the optimization. That is the role of the `notifier`. Optimizing
especially lasts for quite some time if there was no `stopping_condition`,
hence the need to specify one (which could still lead to infinite execution in
practice, but that is another question). At the end of the execution, the
results have to be presented. How they should be presented has to be specified
by requesting the right `results_displayer`.

The function can have any amount of parameters. The range in which they vary
should be specified using `parameters` objects.

`optimization_method`s, `notifier`s and `results_displayer`s might depend on
previous results of the execution. The required results should be specified and
made public information, so that the `memory` object can set itself up in such
a way as to keep only the useful data for all of the constraints to be
satisfied. As of now, the `memory` module does not function.

## History
This library was originally developped as part of a university project (TER,
university of Strasbourg). Last-minute work, hence the half-finished state.
Still, I quite like its architecture.

## Future
I doubt there will be any consequential changes to this framework in the
foreseeable future, as I have other priorities for the time being.

Possible updates:
- Make `memory` function as it should
- Parallelism support, especially GPU
- With above, allow interaction separately from computations (with ability to
manually ask for results up to the current point and so forth)
- Manual interruption of optimization, exportable and importable optimization
tasks (in particular, you should be able to stop a task, and continue in a
later execution)
