#include "notifier_all.hh"

namespace optimizer {
  template <typename ...Args>
  void inform(Args ...parameters, double score) const noexcept override {
    arvernus::message << "f(" << list_parameters(parameters) << ")" << " -> "
      << score;
  }
}
