#ifndef NOTIFIER_NONE_HH
#define NOTIFIER_NONE_HH

#include "notifier.hh"

namespace optimizer {
  template <typename ..Args>
  class notifier_none : public notifier<Args ...> {
  };
};

#endif
