#include "notifier_none.hh"

namespace optimizer {
  template <typename ...Args>
  void notifier_none::inform(
    Args ...parameters, double score
  ) const noexcept override {
  }
}
