#include "notifier_better.hh"

namespace optimizer {
  template <typename ...Args>
  void notifier_better::inform(
    Args ...parameters, double score
  ) const noexcept override {
    arvernus::message << "f(" << list_parameters(parameters) << ")" << " -> "
      << score;
  }
}
