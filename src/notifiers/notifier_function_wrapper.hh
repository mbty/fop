#ifndef OPTIMIZER_NOTIFIER_FUNCTION_WRAPPER_HH
#define OPTIMIZER_NOTIFIER_FUNCTION_WRAPPER_HH

#include <functional>

#include "notifier.hh"

namespace optimizer {
  template <typename ...Args>
  class notifier_function_wrapper : public notifier<Args ...> {
    private:
      std::function<Args ...> function;
  };
}

#endif
