#ifndef OPTIMIZER_NOTIFIER_HH
#define OPTIMIZER_NOTIFIER_HH

#include <tuple>
#include <type_traits>

#include "../../externals/arvernus/src/arvernus.hh"

namespace optimizer {
  template <typename ...Args>
  class notifier {
    public:
      virtual void inform(
        std::tuple<Args...> parameters, double score
      ) noexcept =0;

      virtual ~notifier() {}
  };
}

#endif
