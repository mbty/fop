#ifndef OPTIMIZER_NOTIFIER_NOTIFIER_ALL_HH
#define OPTIMIZER_NOTIFIER_NOTIFIER_ALL_HH

#include <tuple>

#include "auxiliary_functions.hh"
#include "notifier.hh"

namespace optimizer {
  template <typename...Args>
  class notifier_all : public notifier<Args...> {
    virtual void inform(
      std::tuple<Args...> parameters, double score
    ) noexcept override {
      arvernus::message << "f(" << tuple_to_string<Args...>(parameters) << ")"
        << " -> " << score;
    }
  };
}

#endif
