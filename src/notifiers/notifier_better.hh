#ifndef OPTIMIZER_NOTIFIER_NOTIFY_IF_BETTER_HH
#define OPTIMIZER_NOTIFIER_NOTIFY_IF_BETTER_HH

#include "auxiliary_functions.hh"

#include "notifier.hh"

namespace optimizer {
  template <typename ...Args>
  class notifier_better : public notifier<Args ...> {
    public:
      virtual void inform(
        std::tuple<Args...> parameters, double score
      ) noexcept override {
        if (score < best_score || first_time) {
          first_time = false;
          best_score = score;
          arvernus::message << "f(" << tuple_to_string<Args...>(parameters)
            << ")" << " -> " << score;
        }
      }

    private:
      bool first_time = true;
      double best_score;
  };
}

#endif
