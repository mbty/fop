#ifndef OPTIMIZER_FUNCTION_HH
#define OPTIMIZER_FUNCTION_HH

#include <tuple>
#include <type_traits>

namespace optimizer {
  template <typename... Args>
  class function {
    public:
      virtual double operator()(std::tuple<Args...> args) noexcept =0;

      virtual ~function() { }
  };
}

#endif
