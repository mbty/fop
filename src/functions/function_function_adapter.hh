#ifndef OPTIMIZER_FUNCTION_FUNCTION_ADAPTER_HH
#define OPTIMIZER_FUNCTION_FUNCTION_ADAPTER_HH

#include <functional>

#include "function.hh"

namespace optimizer {
  template <typename...Args>
  class function_function_adapter : public function<Args...> {
    public:
      function_function_adapter(std::function<double(Args...)> function)
      : function(function)
      { }

      virtual double operator()(std::tuple<Args...> args) noexcept override {
        return std::apply(function, args);
      }

    private:
      std::function<double(Args...)> function;
  };
}

#endif
