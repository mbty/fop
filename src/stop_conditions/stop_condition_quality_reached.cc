#include "quality_reached.hh"

namespace optimizer {
  quality_reached::quality_reached(double objective)
  : objective(objective)
  { };

  bool quality_reached::should_stop(double quality) noexcept {
    return (quality >= objective);
  }
}
