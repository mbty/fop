#include "stop_condition_step_count.hh"

namespace optimizer {
  stop_condition_step_count::stop_condition_step_count(unsigned int allowed_steps)
  : remaining_steps(allowed_steps)
  { }

  bool stop_condition_step_count::should_stop(
    __attribute__((unused)) double quality
  ) noexcept {
    if (remaining_steps == 0) {
      return true;
    }

    --remaining_steps;
    return false;
  }
}
