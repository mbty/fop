#ifndef OPTIMIZER_STOP_CONDITION_HH
#define OPTIMIZER_STOP_CONDITION_HH

namespace optimizer {
  class stop_condition {
    public:
      virtual bool should_stop(double quality) noexcept =0;

      virtual ~stop_condition() {}
  };
}

#endif
