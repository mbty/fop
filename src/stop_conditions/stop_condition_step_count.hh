#ifndef OPTIMIZER_STOP_CONDITION_STEP_COUNT_HH
#define OPTIMIZER_STOP_CONDITION_STEP_COUNT_HH

#include "stop_condition.hh"

namespace optimizer {
  class stop_condition_step_count : public stop_condition {
    public:
      stop_condition_step_count(unsigned int allowed_steps);

      bool should_stop(double quality) noexcept override;

    private:
      unsigned int remaining_steps;
  };
}

#endif
