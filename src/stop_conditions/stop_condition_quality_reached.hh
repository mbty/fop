#ifndef OPTIMIZER_STOP_CONDITION_QUALITY_REACHED_HH
#define OPTIMIZER_STOP_CONDITION_QUALITY_REACHED_HH

#include "stop_condition.hh"

namespace optimizer {
  class quality_reached : public stop_condition {
    public:
      quality_reached(double objective);

      bool should_stop(double quality) noexcept override;

    private:
      double objective;
  };
}

#endif
