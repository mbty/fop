#ifndef OPTIMIZER_RESULTS_DISPLAYER_FUNCTION_WRAPPER_HH
#define OPTIMIZER_RESULTS_DISPLAYER_FUNCTION_WRAPPER_HH

// TODO activate

#include <functional>

#include "memory.hh"
#include "results_displayer.hh"

namespace optimizer {
  class results_displayer_best : public results_displayer {
    public:
      void display_results() const noexcept =0;
  };
}

#endif
