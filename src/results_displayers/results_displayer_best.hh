#ifndef OPTIMIZER_RESULTS_DISPLAYER_BEST_HH
#define OPTIMIZER_RESULTS_DISPLAYER_BEST_HH

#include "auxiliary_functions.hh"
#include "memory/memory.hh"
#include "results_displayer.hh"

namespace optimizer {
  template <typename...Args>
  class results_displayer_best : public results_displayer<Args...> {
    public:
      virtual void display_results() const noexcept override {
        auto data = this->memory->get_score_indexed_data();

        arvernus::message << "Optimization over with best values "
          << tuple_to_string<Args...>(data.cbegin()->second) << " giving "
          << data.cbegin()->first;
      }
  };
}

#endif
