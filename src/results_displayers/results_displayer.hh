#ifndef OPTIMIZER_RESULTS_DISPLAYER_HH
#define OPTIMIZER_RESULTS_DISPLAYER_HH

#include <type_traits>

#include "memory/memory.hh"

namespace optimizer {
  template <typename...Args>
  class results_displayer {
    public:
      virtual void display_results() const noexcept =0;
      void pass_memory(const class memory<Args...> * new_memory) {
          memory = new_memory;
      }

      virtual ~results_displayer() noexcept { }

    protected:
      const class memory<Args...> *memory;
  };
}

#endif
