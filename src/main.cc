#include "optimizer.hh"

#include "functions/function_function_adapter.hh"
#include "optimization_methods/optimization_method_random.hh"
#include "parameters/parameter_in_range.hh"
#include "stop_conditions/stop_condition_step_count.hh"
#include "notifiers/notifier_better.hh"
#include "results_displayers/results_displayer_best.hh"

using namespace optimizer;

double rosenbrock(double x, double y) {
  double a = 1;
  double b = 100;

  return (a - x)*(a - x) + b*(y - x*x)*(y - x*x);
}

int main() {
  function_function_adapter<double, double> f(&rosenbrock);
  optimization_method_random<double, double> o;
  parameter_in_range x(-5, 5);
  parameter_in_range y(-5, 5);
  stop_condition_step_count s(500000);
  notifier_better<double, double> n;
  results_displayer_best<double, double> r;

  optimize<double, double>(f, {&x, &y}, o, s, n, r);
}
