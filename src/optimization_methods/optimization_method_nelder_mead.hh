#ifndef OPTIMIZER_OPTIMIZATION_METHOD_NELDER_MEAD_HH
#define OPTIMIZER_OPTIMIZATION_METHOD_NELDER_MEAD_HH

#include "optimization_method.hh"
#include "memory/memory.hh"

namespace optimizer {
  template <typename...Args>
  class optimization_method_nelder_mead : public optimization_method<Args...> {
    enum class nelder_mead_state {
      initial_probing,
      centroid,
      reflection,
      redirection,
      expansion,
      contraction,
      shrink,
    };

    public:
      optimization_method_nelder_mead(
        std::array<
          std::array<double, sizeof...(Args)>, sizeof...(Args) + 1
        > initial_values,
        double alpha = 1, double gamma = 2, double rho = 1/2,
        double sigma = 1/2
      )
      : simplex(initial_values), alpha(alpha), gamma(gamma), rho(rho),
        sigma(sigma), index(0)
      { }

      std::array<double, sizeof...(Args)> get_next() {
        std::array<double, sizeof...(Args)> result;

        // TODO keep ordered
        switch (state) {
          case nelder_mead_state::initial_probing:
            if (index > 0) {
              simplex[index - 1].second = get_latest_value;
              if (index == sizeof...(Args) + 2) {
                state = nelder_mead_state::centroid;
                return get_next();
              }
            }
            return simplex[index++].first;

          case nelder_mead_state::centroid:
            order();
            compute_centroid();
            state = nelder_mead_state::reflection;
            return centroid.first;

          case nelder_mead_state::reflection:
            centroid.second = get_latest_value();
            compute_reflection();
            state = nelder_mead_state::redirection;
            return reflection.first;

          case nelder_mead_state::redirection:
            reflection.second = get_latest_value();

            if (smallest.second <= reflection.second < second_biggest.second) {
              // replace bigest with expansion
              state = nelder_mead_state::centroid;
                return get_next();
              }
              else if (reflection.second < smallest.second) {
                state = nelder_mead_state::expansion;
                compute_expansion()
                return expansion.first;
              }
              else {
                state = nelder_mead_state::shrink;
                compute_shrink();
                return shrink.first;
              }

          case nelder_mead_state::expansion:
            expansion.second = get_latest();
            state = nelder_mead_state::centroid;

            if (expansion.second <= reflection.second) {
              // replace biggest with expansion
            }
            else {
              // replace biggest with reflection
            }
            return get_next();

          case nelder_mead_state::contraction:
            contraction.second = get_latest();

            if (contraction.second < biggest.second) {
              state = nelder_mead_state::centroid;
              // replace biggest with contraction
            }
            else {
              state = homothethic_transform;
            }
            return get_next();

          case nelder_mead_state::shrink:
            if (index > 0) {
              if (index == sizeof...(Args) + 1) {
                state = nelder_mead_state::centroid;
              }
            }
            return simplex[index++];
      }
    }

    private:
      double alpha;
      double gamma;
      double rho;
      double sigma;

      size_t index;
      std::array<double, sizeof...(Args)> *latest;

      std::array<std::pair<
        std::array<double, sizeof...(Args)>, double
      >, sizeof...(Args) + 1> simplex;
      nelder_mead_state state;

      std::pair<std::array<double, sizeof...(Args)> *, double> biggest;
      double second_biggest;
      double smallest;

      std::pair<std::array<double, sizeof...(Args)> *, double> next_biggest;
      double next_second_biggest;
      double next_smallest;

      std::pair<std::array<double, sizeof...(Args)>, double> centroid;
      std::pair<std::array<double, sizeof...(Args)>, double> reflection;
      std::pair<std::array<double, sizeof...(Args)>, double> expansion;

      bool fetch_centroid_value;
      bool fetch_reflection_value;
      bool fetch_expansion_value;

      double get_latest_value() {
        return memory->get_time_indexed_data()[
          memory->get_time_indexed_data().size() - 1
        ].second;
      }

      void compute_centroid() {
        centroid.first = std::array<double, sizeof...(Args)>(0);

        for (const auto &coord : simplex) {
          if (&coord != biggest) {
            centroid.first += coord;
          }
        }
      }

      void compute_reflection() {
        reflection.first =
          centroid.first + alpha*(reflection.first - *biggest.first);
      }

      void compute_expansion() {
        expansion.first =
          centroid.first + gamma*(reflection.first - centroid.first);
      }

      void compute_contraction() {
        contraction.first =
          centroid.first + rho*(*biggest.first - centroid.first);
      }

      std::array<double, sizeof...(Args)> compute_shrink(size_t i) {
        = smallest + sigma * ( - smallest); // TODO
      }

      void take_into_account() {
        // Replacements may delete some old biggest/smallest/...
      }

      void order() {
        biggest = next_biggest;
        smallest = next_smallest;
        second_biggest = next_second_biggest;
      }

      void insert() {
      }

      void remove() {
      }
  };
}

#endif
