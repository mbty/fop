#ifndef OPTIMIZER_OPTIMIZATION_METHOD_RANDOM_HH
#define OPTIMIZER_OPTIMIZATION_METHOD_RANDOM_HH

#include <array>
#include <random>

#include "optimization_method.hh"

namespace optimizer {
  template <typename...Args>
  class optimization_method_random : public optimization_method<Args...> {
    public:
      optimization_method_random()
      : distribution(0.0, 1.0)
      {}

      std::array<double, sizeof...(Args)> get_next_values() noexcept override {
        std::array<double, sizeof...(Args)> result;

        for (auto &item : result) {
          item = distribution(generator);
        }

        return result;
      }

    private:
      std::default_random_engine generator;
      std::uniform_real_distribution<double> distribution;
  };
}

#endif
