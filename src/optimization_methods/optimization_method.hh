#ifndef OPTIMIZER_OPTIMIZATION_METHOD_HH
#define OPTIMIZER_OPTIMIZATION_METHOD_HH

#include "memory/memory.hh"

#include <array>

namespace optimizer {
  template <typename...Args>
  class optimization_method {
    public:
      virtual std::array<double, sizeof...(Args)>
        get_next_values() noexcept =0;

      void pass_memory(class memory<Args...> *memory) {
        this->memory = memory;
      }

      virtual ~optimization_method() { }

    private:
      const class memory<Args...> *memory;
  };
}

#endif
