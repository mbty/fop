#ifndef OPTIMIZER_MEMORY_HH
#define OPTIMIZER_MEMORY_HH

// The goal of this class is to keep all the previous stored in order to avoid
// needless data duplication in classes needing access to the results history

#include <map>
#include <utility>
#include <tuple>
#include <vector>

namespace optimizer {
  template <typename...Args>
  class memory {
    public:
      void save(std::tuple<Args...> values, double score) {
        data.insert({score, values});
      }

      const std::multimap<double, std::tuple<Args...>> &
        get_score_indexed_data() const
      {
        return data;
      }

    private:
      std::multimap<double, std::tuple<Args...>> data;
  };
}

#endif
