#ifndef OPTIMIZER_MEMORY_INDEXING_HH
#define OPTIMIZER_MEMORY_INDEXING_HH

// Some elements may need to access elements indexed by something else than
// score to result

// TODO activate

namespace optimizer {
  enum class memory_indexing {
    score_to_result, // always used
    result_to_score,
    time_to_pair,
  };
}

#endif
