#ifndef OPTIMIZER_PROBLEM_HH
#define OPTIMIZER_PROBLEM_HH

#include <array>
#include <tuple>
#include <type_traits>

#include "functions/function.hh"
#include "optimization_methods/optimization_method.hh"
#include "parameters/parameter.hh"
#include "notifiers/notifier.hh"
#include "results_displayers/results_displayer.hh"
#include "stop_conditions/stop_condition.hh"

namespace optimizer {
  template <typename...Args>
  void optimize(
    function<Args...> &f, std::tuple<parameter<Args>*...> parameters,
    class optimization_method<Args...> &optimization_method,
    class stop_condition &stop_condition, class notifier<Args...> &notifier,
    class results_displayer<Args...> &results_displayer
  ) {
    class memory<Args...> memory;
    results_displayer.pass_memory(&memory);
    optimization_method.pass_memory(&memory);

    std::array<double, sizeof...(Args)> current_temp;
    std::tuple<Args...> current_values;
    double current_quality;

    do {
      current_temp = optimization_method.get_next_values();

      std::apply([&current_temp, &current_values] (auto &&...x) {
        size_t i = 0;
        current_values = std::make_tuple(x->get_value(i, current_temp[i])...);
      }, parameters);

      current_quality = f(current_values);

      notifier.inform(current_values, current_quality);
      memory.save(current_values, current_quality);
    } while (!stop_condition.should_stop(current_quality));

    results_displayer.display_results();
  }
}

#endif
