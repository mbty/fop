#include "parameter_in_range.hh"

namespace optimizer {
  parameter_in_range::parameter_in_range(double range_from, double range_to)
  : range_from(range_from), range_to(range_to)
  { }

  double parameter_in_range::get_value_safe(double position) noexcept {
    double value = range_from;
    value += (range_to - range_from) * position;

    return value;
  }
}
