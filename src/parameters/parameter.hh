#ifndef OPTIMIZER_PARAMETER_HH
#define OPTIMIZER_PARAMETER_HH

#include <stdexcept>

#include <iostream>

namespace optimizer {
  template <typename T>
  class parameter {
    public:
      // position should be between 0 and 1
      T get_value(size_t &i, double position);

      virtual ~parameter () {}

    private:
      // position is guaranteed to be between 0 and 1 when this is called
      virtual T get_value_safe(double position) noexcept =0;
  };

  template <typename T>
  T parameter<T>::get_value(size_t &i, double position) {
    ++i;

    if (position < 0 || position > 1) {
      throw std::range_error("parameter position not in range [0, 1]");
    }
    return get_value_safe(position);
  }
}

#endif
