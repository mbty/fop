#ifndef OPTIMIZER_PARAMETER_DOUBLE_IN_RANGE_HH
#define OPTIMIZER_PARAMETER_DOUBLE_IN_RANGE_HH

#include "parameter.hh"

namespace optimizer {
  class parameter_in_range : public parameter<double> {
    public:
      parameter_in_range(double range_from, double range_to);

    private:
      double range_from;
      double range_to;

      virtual double get_value_safe(double position) noexcept override;
  };
}

#endif
