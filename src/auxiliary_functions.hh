#ifndef OPTIMIZER_AUXILIARY_HH
#define OPTIMIZER_AUXILIARY_HH

#include <string>
#include <sstream>
#include <tuple>

#include "../externals/arvernus/src/arvernus.hh"

template <typename...Args>
std::string tuple_to_string(std::tuple<Args...> items) {
  std::stringstream buffer;

  size_t total = sizeof...(Args);
  size_t current = 0;

  std::apply([&buffer, &current, &total] (auto &&...item) {
    ((buffer << item << ", "), ...);
  }, items);

  std::string result = buffer.str();
  // Remove last comma
  result.pop_back();
  result.pop_back();

  return result;
}

#endif
